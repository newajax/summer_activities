
	//rem 适配
	!function (e, i) {
	var t = e.documentElement, n = navigator.userAgent.match(/iphone|ipod|ipad/gi), a = n ? Math.min(i.devicePixelRatio, 3) : 1, m = "orientationchange" in window ? "orientationchange" : "resize";
	    t.dataset.dpr = a;
	    for (var d, l, c = !1, o = e.getElementsByTagName("meta"), r = 0; r < o.length; r++)l = o[r], "viewport" == l.name && (c = !0, d = l);
	    if (c)d.content = "width=device-width,initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0,user-scalable=no"; else {
	        var o = e.createElement("meta");
	        o.name = "viewport", o.content = "width=device-width,initial-scale=1.0,maximum-scale=1.0, minimum-scale=1.0,user-scalable=no", t.firstElementChild.appendChild(o)
	    }
	    var s = function () {
	        var e = t.clientWidth;
	        e / a > 750 && (e = 750 * a), window.remScale = e / 750, t.style.fontSize = 200 * (e / 750) + "px"
	    };
	    s(), e.addEventListener && i.addEventListener(m, s, !1)
	}(document, window);
	
	//input横屏适配
	$(function(){
		var s="";
		if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
			s="1";
		} else if (/(Android)/i.test(navigator.userAgent)) {
			s="2"
		} else {
			s="3";
		};
	
		$(".dialog-inner input").focus(function () {
			if(s=="1"){
					var h=$(window).height();
					$("body,html").css({"overflow":"hidden","height":h+"px"});
					$(".dialog-wrap").css({"position":"absolute","top":"0"});
					window.scrollTo(0, 0);
			}
			if(s=="2"){
				if(location.search.indexOf("flag")==-1) {
				   $(".dialog-wrap").css({"position":"fixed","top":"0"});
				}
			}
		});
		$(".dialog-inner input").blur(function () {
			if(s=="1"){
					$("body,html").css({"overflow":"auto","height":"auto"});
					$(".dialog-content").css("top","50%");
					$(".dialog-wrap").css({"position":"fixed","top":"0"});
			}
			if(s=="2"){
				if(location.search.indexOf("flag")==-1) {
					$(".dialog-wrap").css({"position":"fixed","top":"0"});
				}
			}
		});
	});
	
	//点击登录按钮
	var dialogLogin = $("#dialogLogin");
	$(".btn-gotologin").click(function(){
         dialogLogin.show();
	//今日祝福值已用完弹窗
		//$("#dialogUsertip").show();
	});
	
	//没有账号？快速注册
	$(".gotoregister").click(function(){
		var $dialogWrap = $(this).parents(".dialog-wrap");
		$dialogWrap.hide();
		var dialogReg = $("#dialogReg");
		dialogReg.show();
	});	  
	
	//点击验证码validate code 
	$(".v-code").click(function(){
		if(/^\d{11}$/.test($('#registerphonenum').val())){
			var ifn = arguments.callee;
			var obj = $(this);
			obj.unbind();
			var t = 60;
			(function(){
				if(t > 0){
					obj.html("倒计时"+t--+"s").css("cursor", "default");
					setTimeout(arguments.callee , 1000);
				}else{
					obj.html("重新发送").css("cursor", "pointer");
					obj.bind("click", ifn);
				}
			})();
		}
	});
	//close
	$(".close").click(function(){
		$(".dialog-wrap").hide();	
	});


	
refreshProgress();
function refreshProgress(){
	var total = 360000;
    var amount = $('#amountNowVal').text();
    //获取img的class
 	var className1 = $("#getgift").find('img.img-l');
 	var className2 = $("#getgift").find('img.img-c');
 	var className3 = $("#getgift").find('img.img-r');
    if (isNaN(amount) || amount <=0) {
    	$('#progressInstance i').removeClass('current');
    	$("#mamture_progress dt").css("z-index", "3");    	
	 	
    } else if(amount >0 && amount < 40000){
    	$("#mamture_progress dt.v0").css("z-index", "0");
    	//修改img的src属性
    	className1.attr('src',className1.attr("data-imgoff"));
    	$("img.img-l").click(function(){
    		className1.attr('src',className1.attr("data-imgon"));
    	});
    }else if(amount >=40000 && amount < 180000){
    	$("#mamture_progress dt.v0,#mamture_progress dt.v4").css("z-index", "0");
    	//修改img的src属性
    	className1.attr('src',className1.attr("data-imgoff"));
    	$("img.img-l").click(function(){
    		className1.attr('src',className1.attr("data-imgon"));
    	});
    }else if(amount >=180000 && amount <300000){
    	$("#mamture_progress dt.v0,#mamture_progress dt.v4,#mamture_progress dt.v18").css("z-index", "0");
    	//修改img的src属性
    	className1.attr('src',className1.attr("data-imgoff"));
    	$("img.img-l").click(function(){
    		className1.attr('src',className1.attr("data-imgon"));
    	});
    	//修改img的src属性
    	className2.attr('src',className2.attr("data-imgoff"));
    	$("img.img-c").click(function(){
    		className2.attr('src',className2.attr("data-imgon"));
    	});
    }else if(amount >=300000 && amount <360000){
    	$("#mamture_progress dt.v0,#mamture_progress dt.v4,#mamture_progress dt.v18,#mamture_progress dt.v30").css("z-index", "0");
    	//修改img的src属性
    	className1.attr('src',className1.attr("data-imgoff"));
    	$("img.img-l").click(function(){
    		className1.attr('src',className1.attr("data-imgon"));
    	});
    	//修改img的src属性
    	className2.attr('src',className2.attr("data-imgoff"));
    	$("img.img-c").click(function(){
    		className2.attr('src',className2.attr("data-imgon"));
    	});
    	//修改img的src属性
    	className3.attr('src',className3.attr("data-imgoff"));
    	$("img.img-r").click(function(){
    		className3.attr('src',className3.attr("data-imgon"));
    	});
    }else if(amount >=300000 && amount <360000){
    	$("#mamture_progress dt").css("z-index", "0");
    	//修改img的src属性
    	className1.attr('src',className1.attr("data-imgoff"));
    	$("img.img-l").click(function(){
    		className1.attr('src',className1.attr("data-imgon"));
    	});
    	//修改img的src属性
    	className2.attr('src',className2.attr("data-imgoff"));
    	$("img.img-c").click(function(){
    		className2.attr('src',className2.attr("data-imgon"));
    	});
    	//修改img的src属性
    	className3.attr('src',className3.attr("data-imgoff"));
    	$("img.img-r").click(function(){
    		className3.attr('src',className3.attr("data-imgon"));
    	});

    }else {
    	$('#progressInstance i').addClass('current');
    }
    var ratio = ratio = (amount/total) * 100;
	$('#progressInstance').animate({width: ratio+'%'},'easing');
}

//领取
$('.amount-content .btn-style2').on('click',function(){
	var amount = parseInt($('#amountNowVal').text());
	$('#amountNowVal').text(amount + 5);
	$(this).removeClass('btn-style2').addClass('btn-style1').off('click').text('已助力');
	refreshProgress();
})

